"use strict"

var removeButton = {
    props: ['identifier'],
    template: '<button @click="del()">&times</button>',
    methods: {
        del: function () {
            app.posts.splice(this.identifier, 1);
        },
    },
}

// {{id}} --- {{content}}
var articleWindow = {
    props: ['id'],
    data() {
        return {
            content: "hello this is some cool text",
            editor: "",
            edit:true,
            // edit2:""
        }
    },
    template: `
    <div class="thing">
    <div v-if="!this.edit"><input  type="text" name="textbox"v-model="content"><button @click="editorSave()">save</button></div>
    <div v-else @dblclick="editorLaunch">{{content}}</div>
    <div class="close" @click="$emit('windowCloseEmit')">&times</div>
    </div>
    `, 
    methods: {
        editorLaunch: function() {
            this.edit = false;
        },
        editorSave: function () {
            this.edit = true;

        }
    },
}



Vue.component('card', {
    props: ['article', 'index'],
    components: {
        'removeButton': removeButton,
        'articleWindow': articleWindow
    },
    data() {
        return {
            open: false,
        }
    },
    template: `
    <article>
        <div class="clicker" @click = "windowOpen" ></div>
        {{ article.id }}{{ article.cat }}
        <removeButton :identifier="index"></removeButton>
        <div class="title">{{ article.title }}</div>

        <articleWindow @windowCloseEmit="windowClose" v-if="open" :id="article.id"></articleWindow>
    </article>`,
    methods: {
        windowClose: function () {
            this.open = false;
            console.log(this.open);
        },
        windowOpen: function() {
            this.open = true;
            console.log(this.open);
        },
    }
})


var app = new Vue({
    el: '#app',
    data: {
        title: "",
        cat: "",
        q: "",
        message: 'Hello t!',
        posts: [{
                id: 0,
                title: "a",
                cat: "A"
            },
            {
                id: 1,
                title: "b",
                cat: "B"
            },
            {
                id: 2,
                title: "wertgwert",
                cat: "A"
            },
            {
                id: 3,
                title: "cwa qef",
                cat: "B"
            },

        ]
    },
    methods: {
        addtitle: function () {
            this.posts.push({
                id: this.posts.length == 0 ? 1 :(this.posts[this.posts.length - 1].id + 1),
                title: this.title.toString(),
                cat: this.cat.toString()
            });
        },
        del: function (index) {
            this.posts.splice(index, 1);

        },
        search: function () {
            console.log(this.title);
            
            if (this.q == "") {
                console.log("hey");
                
                return true;

            } else if (this.title.toString().includes(this.q.toString())) {
                return true;
            }
        }
    },
})